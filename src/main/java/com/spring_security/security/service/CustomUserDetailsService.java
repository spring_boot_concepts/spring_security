package com.spring_security.security.service;


import com.spring_security.security.model.User;
import com.spring_security.security.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    UserRepo userRepo;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user=userRepo.findByUsername(username);
        CustomUserDetails userDetails=null;
        if(user!=null){
            userDetails=new CustomUserDetails();
            userDetails.setUser(user);
        }
        else{
            throw new UsernameNotFoundException("User not exist with"+username);
        }
        return null;
    }
}
