package com.spring_security.security.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app")
public class AppController {
    @RequestMapping("/status")
    public String getStatus(){
        return "Its still being processed";
    }
}
