package com.spring_security.security.controller;

import com.spring_security.security.model.User;
import com.spring_security.security.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AdminController {

    @Autowired
    UserRepo userRepo;
    @Autowired
    BCryptPasswordEncoder pwdEncoder;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping("/addUsers")
    public String addUserByAdmin(@RequestBody User user){
        String encodePwd=pwdEncoder.encode(user.getPassword());
        user.setPassword(encodePwd);
        userRepo.save(user);
        return "User was added Successfully";
    }
}
